document.addEventListener("DOMContentLoaded", function () {
  const footer = document.querySelector(".footer");
  const backBtn = document.getElementById("backBtn");
  const nextBtn = document.getElementById("nextBtn");
  const section1 = document.querySelector(".section1");
  const section2 = document.querySelector(".section2");
  const section3 = document.querySelector(".section3");
  const section4 = document.querySelector(".section4");
  const section5 = document.querySelector(".section5");

  const nameInput = document.getElementById("name");
  const emailInput = document.getElementById("email");
  const phoneInput = document.getElementById("phone");

  const nameError = document.querySelector(".label-for-name .error");
  const emailError = document.querySelector(".label-for-email .error");
  const phoneError = document.querySelector(".label-for-phone .error");

  const circle1 = document.getElementById("circle1");
  const circle2 = document.getElementById("circle2");
  const circle3 = document.getElementById("circle3");
  const circle4 = document.getElementById("circle4");

  const arcadePrice = document.querySelector(".arcadePrice");
  const advancePrice = document.querySelector(".advancePrice");
  const proPrice = document.querySelector(".proPrice");
  const checkbox = document.getElementById("checkbox");

  const plan = document.querySelectorAll(".plan");
  const planRadio = document.querySelectorAll(".planRadio");

  const onlineServicePrice = document.querySelector(".onlineServicePrice");
  const largeStoragePrice = document.querySelector(".largeStoragePrice");
  const customizableProfilePrice = document.querySelector(
    ".customizableProfilePrice"
  );

  const duration = document.querySelector(".section4 #duration");
  const addOnName = document.querySelector(".section4 .addOnName");
  const amountPlan = document.querySelector(".section4 #amountPlan");
  const durationTotal = document.querySelector(".section4 .durationTotal");
  const planAddOns = document.querySelector(".section4 .planAddOns");
  const costAmount = document.querySelector(".section4 .costAmount");
  const changeLink = document.querySelector(".section4 .planTitle a");

  backBtn.style.display = "none";
  nameError.style.display = "none";
  emailError.style.display = "none";
  phoneError.style.display = "none";

  function showBackButton() {
    backBtn.style.display = "block";
  }

  function validateFields() {
    let isValid = true;

    if (nameInput.value.trim() === "") {
      nameError.style.display = "block";
      isValid = false;
    } else {
      nameError.style.display = "none";
    }

    if (emailInput.value.trim() === "") {
      emailError.style.display = "block";
      isValid = false;
    } else {
      emailError.style.display = "none";
    }

    if (phoneInput.value.trim() === "") {
      phoneError.style.display = "block";
      isValid = false;
    } else {
      phoneError.style.display = "none";
    }

    return isValid;
  }

  circle1.style.backgroundColor = "var(--Light-blue)";
  circle1.style.color = "var(--Marine-blue)";

  const arcadePlan = document.querySelector(".arcade.plan");
  const advancePlan = document.querySelector(".advance.plan");
  const proPlan = document.querySelector(".pro.plan");

  arcadePlan.addEventListener("click", function () {
    const arcadeRadio = arcadePlan.querySelector(".planRadio");
    arcadeRadio.click();
    addOnName.textContent = "Arcade";
    if (checkbox.checked) {
      amountPlan.textContent = "$90/yr";
    } else {
      amountPlan.textContent = "$9/mo";
    }
    updateTotal();
  });

  advancePlan.addEventListener("click", function () {
    const advanceRadio = advancePlan.querySelector(".planRadio");
    advanceRadio.click();
    addOnName.textContent = "Advance";
    if (checkbox.checked) {
      amountPlan.textContent = "$120/yr";
    } else {
      amountPlan.textContent = "$12/mo";
    }
    updateTotal();
  });

  proPlan.addEventListener("click", function () {
    const proRadio = proPlan.querySelector(".planRadio");
    proRadio.click();
    addOnName.textContent = "Pro";
    if (checkbox.checked) {
      amountPlan.textContent = "$150/yr";
    } else {
      amountPlan.textContent = "$15/mo";
    }
    updateTotal();
  });

  plan.forEach((planElement) => {
    planElement.addEventListener("click", function () {
      const input = planElement.querySelector("input[type='radio']");
      input.style.backgroundColor = "var(--Light-blue)";
    });
  });

  const checkboxes = document.querySelectorAll(".pick input[type='checkbox']");
  checkboxes.forEach(function (checkbox) {
    checkbox.addEventListener("change", function () {
      const addonName =
        checkbox.parentElement.querySelector("label").textContent;
      const addonPrice = checkbox.parentElement.querySelector("h5").textContent;

      if (checkbox.checked) {
        const addonItem = document.createElement("div");
        addonItem.classList.add("addon-item");

        const addonNameElement = document.createElement("span");
        addonNameElement.textContent = addonName;
        addonNameElement.classList.add("addon-name");

        const addonPriceElement = document.createElement("span");
        addonPriceElement.textContent = addonPrice;
        addonPriceElement.classList.add("addon-price");

        addonItem.appendChild(addonNameElement);
        addonItem.appendChild(addonPriceElement);

        planAddOns.appendChild(addonItem);
        updateTotal();
      } else {
        const items = planAddOns.querySelectorAll("div");
        items.forEach(function (item) {
          if (item.textContent.includes(addonName)) {
            item.remove();
          }
        });
        updateTotal();
      }
    });
  });

  function updateTotal() {
    let total = 0;
    let timeUnit = "/mo";

    if (checkbox.checked) {
      timeUnit = "/yr";
    }
    if (amountPlan.textContent.includes("mo")) {
      total += parseFloat(
        amountPlan.textContent.replace("$", "").replace("/mo", "")
      );
    } else {
      total += parseFloat(
        amountPlan.textContent.replace("$", "").replace("/yr", "")
      );
    }
    const addonItems = planAddOns.querySelectorAll("div");
    addonItems.forEach(function (item) {
      const price = parseFloat(
        item.textContent.match(/\$\d+/)[0].replace("$", "")
      );
      total += price;
    });
    costAmount.textContent = `$${total}${timeUnit}`;
  }

  changeLink.addEventListener("click", function (e) {
    e.preventDefault();
    section4.style.display = "none";
    section2.style.display = "flex";
    circle4.style.backgroundColor = "";
    circle4.style.color = "white";
    circle2.style.backgroundColor = "var(--Light-blue)";
    circle2.style.color = "var(--Marine-blue)";
    nextBtn.textContent = "Next";
    updateTotal();
  });

  nextBtn.addEventListener("click", function () {
    if (section1.style.display !== "none") {
      if (validateFields()) {
        circle1.style.backgroundColor = "";
        circle1.style.color = "white";
        section1.style.display = "none";
        section2.style.display = "flex";
        showBackButton();
        circle2.style.backgroundColor = "var(--Light-blue)";
        circle2.style.color = "var(--Marine-blue)";
      }
    } else if (section2.style.display !== "none") {
      const selectedPlan = document.querySelector(
        '.section2 input[name="plan"]:checked'
      );
      if (!selectedPlan) {
        alert("Please select a plan.");
        return;
      } else {
        section2.style.display = "none";
        section3.style.display = "flex";
        circle2.style.backgroundColor = "";
        circle2.style.color = "white";
        circle3.style.backgroundColor = "var(--Light-blue)";
        circle3.style.color = "var(--Marine-blue)";
      }
    } else if (section3.style.display !== "none") {
      nextBtn.textContent = "Confirm";
      section3.style.display = "none";
      section4.style.display = "flex";
      circle3.style.backgroundColor = "";
      circle3.style.color = "white";
      circle4.style.backgroundColor = "var(--Light-blue)";
      circle4.style.color = "var(--Marine-blue)";
      updateTotal();
    } else if (section4.style.display !== "none") {
      section4.style.display = "none";
      section5.style.display = "flex";
      nextBtn.style.display = "none";
      backBtn.style.display = "none";
      footer.style.backgroundColor = "";
    }
  });

  backBtn.addEventListener("click", function () {
    if (section4.style.display !== "none") {
      section4.style.display = "none";
      section3.style.display = "flex";
      nextBtn.textContent = "Next";
      circle4.style.backgroundColor = "";
      circle4.style.color = "white";
      circle3.style.backgroundColor = "var(--Light-blue)";
      circle3.style.color = "var(--Marine-blue)";
    } else if (section3.style.display !== "none") {
      section3.style.display = "none";
      section2.style.display = "flex";
      circle3.style.backgroundColor = "";
      circle3.style.color = "white";
      circle2.style.backgroundColor = "var(--Light-blue)";
      circle2.style.color = "var(--Marine-blue)";
    } else if (section2.style.display !== "none") {
      section2.style.display = "none";
      section1.style.display = "flex";
      backBtn.style.display = "none";
      circle2.style.backgroundColor = "";
      circle2.style.color = "white";
      circle1.style.backgroundColor = "var(--Light-blue)";
      circle1.style.color = "var(--Marine-blue)";
    }
  });

  checkbox.addEventListener("change", function () {
    planAddOns.innerHTML = "";
    updateTotal();
    const section3Checkboxes = document.querySelectorAll(
      ".section3 input[type='checkbox']"
    );
    section3Checkboxes.forEach(function (checkbox) {
      checkbox.checked = false;
    });
    if (checkbox.checked) {
      arcadePrice.textContent = "$90/yr";
      const spanArcade = document.createElement("span");
      spanArcade.textContent = " 2 months free";
      spanArcade.style.color = "var(--Marine-blue)";
      spanArcade.style.display = "block";
      arcadePrice.appendChild(spanArcade);
      advancePrice.textContent = "$120/yr";
      const spanAdvance = document.createElement("span");
      spanAdvance.textContent = " 2 months free";
      spanAdvance.style.color = "var(--Marine-blue)";
      spanAdvance.style.display = "block";
      advancePrice.appendChild(spanAdvance);
      proPrice.textContent = "$150/yr";
      const spanPro = document.createElement("span");
      spanPro.textContent = " 2 months free";
      spanPro.style.color = "var(--Marine-blue)";
      spanPro.style.display = "block";
      proPrice.appendChild(spanPro);
      onlineServicePrice.textContent = "$10/yr";
      largeStoragePrice.textContent = "$20/yr";
      customizableProfilePrice.textContent = "$20/yr";
      duration.textContent = "(Yearly)";
      durationTotal.textContent = "(per year)";
    } else {
      arcadePrice.textContent = "$9/mo";
      advancePrice.textContent = "$12/mo";
      proPrice.textContent = "$15/mo";
      onlineServicePrice.textContent = "$1/mo";
      largeStoragePrice.textContent = "$2/mo";
      customizableProfilePrice.textContent = "$2/mo";
      duration.textContent = "(Monthly)";
      durationTotal.textContent = "(per month)";

      updateTotal();
    }
    updateTotal();
  });

  const circles = [
    document.getElementById("circle1"),
    document.getElementById("circle2"),
    document.getElementById("circle3"),
    document.getElementById("circle4"),
  ];

  const stepTexts = ["YOUR INFO", "SELECT PLAN", "ADD-ONS", "SUMMARY"];

  const addStepText = () => {
    circles.forEach((circle, index) => {
      const stepDiv = document.createElement("div");
      stepDiv.classList.add("step-container");
      circle.appendChild(stepDiv);
      const stepNumber = document.createElement("span");
      stepNumber.textContent = `Step ${index + 1} `;
      stepDiv.appendChild(stepNumber);

      const stepText = document.createElement("span");
      stepText.textContent = stepTexts[index];
      stepDiv.appendChild(stepText);
    });
  };

  const removeStepText = () => {
    const stepContainers = document.querySelectorAll(".step-container");
    stepContainers.forEach((stepContainer) => stepContainer.remove());
  };

  addStepText();

  window.addEventListener("resize", function () {
    if (window.innerWidth >= 1024) {
      addStepText();
    } else {
      removeStepText();
    }
  });
});
